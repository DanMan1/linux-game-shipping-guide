---
title: "Middleware"
category: "Recommended Tools"
order: 2
---

## SDL

Quoting the official website:

> Simple DirectMedia Layer is a cross-platform development library designed to
provide low level access to audio, keyboard, mouse,  joystick, and graphics
hardware via OpenGL and Direct3D. It is used by video playback software,
emulators, and popular games including [Valve](http://valvesoftware.com/)'s
award winning catalog and many [Humble Bundle](https://www.humblebundle.com/)
games.

SDL is a library commonly used to simplify the work of creating cross platform
games or game engines, and supports Windows, Mac OS X, Linux, iOS, and Android.
SDL has been in development for a long time and is a highly effective solution
for abstracting the details of how to perform common OS specific tasks. This is
especially useful for porting a game to Linux with an existing codebase written
from scratch.

For more information on how to use SDL, consult the
[official SDL wiki](https://wiki.libsdl.org/).

## GLFW

Quoting the official website:

> GLFW is an Open Source, multi-platform library for OpenGL, OpenGL ES and
Vulkan development on the desktop. It provides a simple API for creating
windows, contexts and surfaces, receiving input and events. GLFW is written in
C and supports Windows, macOS, X11 and Wayland. GLFW is licensed under the
zlib/libpng license.

GLFW, like SDL, is a library used to simplify the task of cross platform game
development, by abstracting the details of the underlying OS with a common API.

GLFW is very simple to setup and use, and in addition to C and C++, has many
bindings are available for GLFW in other languages, such as C#, Java,
Node.js, Rust and Python.

For more information on GLFW, check out the [official website][1].

## LunarG - The Official Vulkan SDK

Vulkan is a cross-platform industry standard enabling developers to target a
wide range of devices with the same graphics API, developed by Khronos Group,
the industry body consisting of major industry players such as Intel, Microsoft,
NVIDIA, AMD, Google, Apple, Samsung, Sony, Valve, ARM Holdings, Epic Games,
Electronic Arts, Adobe and Nintendo just to name a few.

If you are a C++ game developer looking to get started with porting your game or
game engine from DirectX to Vulkan, to enjoy the benefits of the high
performance cross platform API, here are three good places to start:

 1. Grab [LunarG][2], the Official Vulkan SDK by Khronos Group. Khronos Group put out
 a new SDK regularly with updated debugging tools, examples and documentation to
 make your life easier.
 2. The [Khronos Group official Youtube channel][4], which contains many videos
 with talks from experienced developers from the industry, with tips on how to
 understand Vulkan, with advice on how to get started, pitfalls to avoid,
 and best practices to adopt.
 3. The most popular Vulkan tutorial on the web, [vulkan-tutorial.com][3].


## Anti-Cheat Software

Server-side anti-cheat solutions are recommended because this has benefits for
a developer who intends to expand their reach to include the Linux platform:

* Reduced complexity in your client software since you're independent from the user's OS
* Client-side anti-cheat technology used for Windows games isn't always available for Linux, too
* Avoids user concerns about privacy and security arising from running low-level software on their systems

Even if you don't intend to officially release on the Linux platform, you'd make
it easier to run your game through Wine/Proton, since you don't create an insurmountable
hurdle by using client-side anti-cheat technologies, which are often incompatible with Wine/Proton.

So all this could boost the positive reception of your game and could be leveraged
as a selling point to strongly promote your game to Linux gamers.

Some examples we know of:

### FairFight

In their own words:

> FairFight® is a non-invasive, customizable, server side anti-cheat engine using agnostic technology that operates in real time.
> It does not reside on the player's computer or the game server, and does not examine the player's devices or perpetually look for the latest hacks.

Find out more on their [official website](https://www.i3d.net/anti-cheat-software/).

### Anybrain

From their FAQ:

> Our anti-cheat is based on player behaviour analysis. We have developed our
> platform with a HCI (Human Computer Interaction) approach based on
> machine learning algorithms capable of learning from the game and player, 
> understanding gameplay and detecting frauds by interpreting abnormal behaviours.
>
> We only analyze data during your gameplay, and only game data inputs.
> Our SDK does not reside on the player's computer and does not examine the players' devices,
> nor what’s installed on them. Our technology is also game and platform agnostic.

Find out more on their [official website](https://anybrain.gg/).


[1]: https://www.glfw.org/
[2]: https://www.lunarg.com/vulkan-sdk/
[3]: https://vulkan-tutorial.com/
[4]: https://www.youtube.com/user/khronosgroup
