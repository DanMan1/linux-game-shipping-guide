---
title: "FAQ"
category: Basics
order: 1
---

## "What will I find within this guide?"

Helpful information for existing game developers considering developing games
for Linux.

## "What is Linux?"

The term Linux is used colloquially to refer to an entire family of OSes that
use Linux as their kernel.

## "What is a operating system kernel?"

The main component of an operating system. It controls the hardware and allows
applications to run.

## "What's a Linux distribution?"

A Linux distribution is a Linux based operating system, made up of a collection
of software combined with the Linux kernel. A typical Linux distribution
comprises a Linux kernel, additional software (including tools from the
[GNU project][1]), and a desktop environment.

## "What is Proton?"

Proton is a software compatibility layer for running native Windows games on
Linux, reinterpreting native Windows and DirectX API calls to Linux compatible
equivalents. Proton is developed by Valve and incorporates components such as
[Wine][2] and [DXVK][3] and is available via the Linux version of Steam.

## "What is a Desktop Environment?"

The graphical user interface of a Linux distribution. Most desktop environments
will also include a set of integrated applications. The desktop environment and
these applications can generally be customised by the user.

## "Who uses Linux?"

Linux OSes are free to download and install, and are used by millions of people
around the world. The Linux kernel itself can be found everywhere, from
supercomputers, to servers, to PCs, to smartphones, bluray players, and game consoles.

It can also be found in the popular gaming device known as 'Steam Deck', created
and sold by Valve, a handheld gaming PC that runs 'SteamOS', a Linux based OS,
that runs native Linux games, and Windows games via Proton.

Linux is popular among PC users who enjoy the level of customisation that Linux
offers. It is also popular for its ability to run on a wide range of hardware,
including older hardware.

According to Steam's official stats, as of August 2023, an estimated 1.82% of Steam's
userbase are gaming on a PC with a Linux based OS, putting Linux slightly ahead of
MacOS (at 1.57%).

Based on Steam's estimated 132 million monthly active user count [(source)][4],
there is an estimated roughly 2.4 million monthly active gamers on Steam alone using a
Linux based OS on their PC.

## "What is the equivalent of an .exe on Linux?"

An executable file on Windows, commonly seen as an .exe file, is officially
known as Portable Executable format (also known as the PE format).

The nearest equivalent on Linux is the Executable and Linkable Format, (also
known as the ELF format).

There is no standard file extension for this format and these executable files
are often provided without an extension at all. Generally speaking Linux is far
less dependent on file extensions than Windows. Whether or not Linux considers a
file executable is defined instead by a permission set per file.

## "How are software and games distributed on Linux?"

On Windows, it's common practice to deliver software to users by creating an
installer that copies the application's required files into a directory, usually
something like `C:\Program Files\[Developer]\[Application Name]\`.

It's also up to that developer to ensure their software doesn't spread files all
over the user's system, and also to create an uninstaller that removes the
software easily as well.

This is _not_ how software is usually delivered on Linux.

There are several common methods to cover:
  * Package Managers
  * Flatpak
  * AppImage
  * Snap
  * Steam
  * Tarballs

## "What's a package manager?"

Typically on Linux, software is shipped via some form of package manager. A
package manager is an application that manages installing, upgrading and
removing packages of files from a system.

All Linux distributions come with some kind of package manager. Examples include:

  * Apt (for Debian, Ubuntu, Linux Mint, Pop!\_OS)
  * Pacman (Arch, Manjaro)
  * RPM (Fedora, CentOS)

Package managers can install packages from package files, or from online
repositories. For example, Apt can load packages from `.deb` files, which can be
installed to the user's system by double clicking on them to open them.

Online repositories are a convenience on Linux without a Windows parallel.
Distribution maintainers maintain repositories of software compiled and packaged
explicitly for their distribution, sometimes even customising that software to
better integrate with their distribution (if permitted to), and then offer it
for download and installation via software managers.

## "What is Flatpak?"

Flatpak can be thought of as a universal package manager, but just with a
little extra special functionality. Flatpak, and its online repository
'Flathub', is available for all major Linux distributions.

Flatpak applications are also provided in a sandboxed container, and come with
their own runtimes and dependencies, to ensure the application runs consistently
the same way across all Linux distributions.

You can easily add your own application to Flathub if you wish, to make it
available to all Linux distributions that support Flatpak.

## "What is AppImage?"

AppImage is a container format for distributing an entire application and all of
its dependencies and runtime as a single file that the user can store on their
PC and run with a double click.

Many Linux distros now offer AppImage integration to automatically add launcher
icons for an AppImage application when it is run.

This is a very easy way to distribute software on Linux and ensure that it runs
correctly, as you can provide the AppImage of your software from your website
as a simple download.

## "What is Snap?"

Snaps are universal Linux packages that package an application and its
dependencies into one compressed file. They are sandboxed from the underlying OS
with control over which system resources can be accessed.

They are distributed, and updated, through the Snap Store. On supported distros
installation and updating of the application can be integrated into the normal
method that a user would use to find and install software.

## "Steam for Distributing Software?"

Steam is without a doubt the most popular PC gaming platform on Linux and
an ideal platform for distributing games on Linux. Shipping games to Linux via
Steam is not that different to shipping games to Windows, and comes with the
added advantage of the Steam Runtime, to ensure broad compatibility across Linux
distributions for your game.

Steam is also suitable for shipping proprietary commercial software to Linux,
and is used already for this purpose. An example of an application sold on Steam
is Substance Painter.

## "What's a Tarball?"

In some situations, a developer may choose to ship their software on Linux by
simply providing a compressed folder with the software inside and leave it up to
the user to uncompress that folder, store it somewhere safe, then create a
launcher for the main executable file inside of the folder.

When software is shipped via this method, it is a .tar.gz compressed archive, or
sometimes simply a .zip.

Regardless of which format is used, if the application inside requires
executable permission to run, ensure that file permissions are preserved, with a
quick test, to avoid inconveniencing your users.

> Note: A .tar file is a directory of files packed into a single archive, but
the .tar format doesn't offer any compression. The .gz format can compress a
single file with the GZip compression algorithm, but can not store multiple
files. So a .tar.gz file is a directory of files compressed, much like a .zip
file.

[1]: https://en.wikipedia.org/wiki/GNU_Project
[2]: https://www.winehq.org
[3]: https://github.com/doitsujin/dxvk
[4]: https://store.steampowered.com/news/group/4145017/view/3133946090937137590
