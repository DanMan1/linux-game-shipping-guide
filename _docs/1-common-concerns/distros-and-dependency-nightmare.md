---
title: "Distros & Dependency Nightmare"
category: Common Concerns
order: 3
---

> **Concern: "Managing dependencies for Linux is a nightmare because there are
many Linux distributions and they each have different libraries, desktop
environments and kernels."**

This concern is often cited by developers when considering supporting Linux, and
backed up by stories from other developers who have experienced issues
supporting Linux relating to how they handle managing their dependencies.

If managed well, managing dependencies and multiple-distro support can be a
trivial matter on Linux.

## Kernel Versions

With Linux distributions often updating their kernel version, it's sometimes an
[expressed concern or question,][1] "What kernel version should my game target?".

The answer is: You don't need to worry about Linux kernel versions.

The first reason why is because most programs do not depend or interact
directly with kernel interfaces.

The second reason why is because Linux kernel development operates under a
strict mantra of: "We do not break userspace".

[In the words of Linus Torvalds himself:][2]

> We care about user-space interfaces to an insane degree. We go to extreme
lengths to maintain even badly designed or unintentional interfaces. Breaking
user programs simply isn't acceptable. (…) We know that people use old binaries
for years and years, and that making a new release doesn't mean that you can
just throw that out. You can trust us.

Newer Linux kernel versions may expose new interfaces, but old interfaces remain
supported.

## Managing Dependencies

For compiled projects (such as C++ projects) where you will need to be managing
your dependencies yourself, Ethan Lee, a professional Linux porter, has written
a short essay on how he approaches packaging games for Linux, [available here.][3]

The guide includes also a link to Ethan's informative [presentation][4] that
contains valuable information, and which Ethan recommends watching first. In
Ethan's guide, he describes how to find the dependencies your software needs
using the simple `ldd` command, then provides advice on determining which ones
should be packaged with your software.

## Take advantage of the Steam Runtime

Steam for Linux ships with its own runtime environment called the [Steam Runtime][5].
By default, Steam launches all Steam Applications within the runtime environment,
which is based off Ubuntu 12.04. But you can also use it without Steam to ship your
own games, as long as you honor the contained licenses.

By targeting this runtime, you can ensure your application runs with a
consistent available set of libraries on all platforms. Any dependencies that
you require that are not available in the Steam Runtime, you should package as
.so files with your game as per normal.

See the [Steamworks documentation][6] for more information about building games 
for Steam on Linux, or [this tutorial][14] for a quick start on how to use the 
runtime even without Steam.

## Use AppImage, Flatpak, or Snap

[AppImage][7], [Flatpak][8] and [Snap][15] are container formats for distributing software
on Linux and allow for packaging an entire runtime with your application. This
ensures your application works exactly as expected across all Linux
distributions that support AppImage and Flatpak. Consider packaging your game
into one of these formats.

If you are not sure which format to go with, this author recommends AppImage for
its simplicity.

## Take advantage of existing game engines & middleware that strongly support Linux

This is less of an option if you already have a game developed, but if you are
still in the planning stage for a game, you will find it much easier to support
Linux by choosing technologies that are known to easily support multiple
platforms including Linux, such as Unreal Engine, Unity, Godot, SDL, and so on.
Read the related 'Recommended Tools' section for further advice on specific
tools, technologies, engines and software.

## Target a specific Linux Distribution

If you are still worried about potential issues over library dependencies and
compatibility issues of running your game across multiple Linux distros, or the
burden of support, there's another alternative: simply state in the requirements
for your game that you only officially support one Linux distribution.
The current popular choice is the most popular Linux distribution on Steam,
Ubuntu.

One of the best things about Linux is the freedom to use it how you like, to
customise Linux and go nuts with modifying it, as many Linux users will tell
you. But at the same time, those Linux users will not be terribly surprised or
disappointed if they discover your game, that explicitly states it targets
'Ubuntu 16.04 & Up', doesn't work on their rooted ARM CPU powered smart-toaster
running Raspbian.

As long as the minimum requirements are clearly stated, your customers will
happily accept this. Chances are, your game will run fine on all popular Linux
distributions by targeting Ubuntu, and many Linux gamers will enjoy your game
even if they don't use Ubuntu.

## Unofficial Linux Builds

If you are still not confident about the stability or optimisation of the Linux
version of your game, but are interested in providing it anyway, you do have the
option of providing a Linux build as an unofficial build. On Steam, for example,
there is the option to provide a native Linux build of a game, that can be
installed the same way as an official version, without indicating on the Steam
store page that there is official native Linux support.

Many games already do this, such as:

* [Thomas Was Alone][9]
* [Faerie Solitaire][10]
* [Thea: The Awakening][11]
* [SCP: Secret Laboratory][12]
* [The Swindle][13]

If you are honest with the Linux gaming community and inform them that the
Linux support is unofficial or 'experimental', they will understand and likely
appreciate even unofficial support. Later on, if the Linux build is a success,
and runs well, you can make the support official.

[1]: https://twitter.com/olafurw/status/1155560620433035264?s=19
[2]: https://yarchive.net/comp/linux/gcc_vs_kernel_stability.html
[3]: https://gist.github.com/flibitijibibo/b67910842ab95bb3decdf89d1502de88
[4]: https://www.youtube.com/watch?v=B83CWUh0Log
[5]: https://github.com/ValveSoftware/steam-runtime
[6]: https://partner.steamgames.com/doc/store/application/platforms/linux
[7]: https://appimage.org/
[8]: https://flatpak.org/
[9]: https://store.steampowered.com/app/220780/Thomas_Was_Alone/
[10]: https://store.steampowered.com/app/38600/Faerie_Solitaire/
[11]: https://store.steampowered.com/app/378720/Thea_The_Awakening/
[12]: https://store.steampowered.com/app/700330/SCP_Secret_Laboratory/
[13]: https://store.steampowered.com/app/369110/The_Swindle/
[14]: https://jorgen.tjer.no/post/2014/05/28/steam-runtime-without-steam/
[15]: https://snapcraft.io/
