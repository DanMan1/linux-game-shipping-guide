---
title: "High Volume Support Requests"
category: Common Concerns
order: 1
---

**"I've heard unpleasant stories from other developers about supporting Linux,
such as the disproportionally high volume of support requests."**

Generally speaking you will not receive disproportionally high volumes of
support requests from Linux gamers for a well supported port.

Some developers have indeed had bad experiences in shipping games to Linux. But,
many other developers have expressed [very positive experiences][1] as well.

But there is a basis to the claims of negative experiences in the past, of
support requests from Linux users being noticeably *different* to the support
requests from Windows and Mac OS users, for three main reasons.

## Linux Has Evolved Rapidly Over The Past Decade

Anyone will tell you, a few years is a long time in the world of technology,
and many of the stories of unpleasant experiences in supporting Linux are from
many years ago.

In past years, Linux desktop OSes were offered a vastly different level of
official driver support from Intel, AMD and NVIDIA. Official support from game
engines usually wasn't available, and was much poorer even when it was.
Prior to 2013, Steam was not available on Linux, and Valve's support to Linux
has drastically improved since the initial launch.

Linux gaming in 2019, is barely recognisable in comparison to 2009.

## Windows Developers Unfamiliar With Linux

Linux is very different to Windows. The technology, tools, methods of packaging,
and shipping games are all different to Windows. Even the market itself and the
type of customers are very different.

This can throw off some developers, who are only experienced with shipping games
to Windows, and approach the Linux platform expecting something nearly
identical, just with a different name. Developers who approach Linux with this
expectation are usually disappointed, and have a bad experience.

This guide hopefully will adjust your expectations, so that you know what to
expect from supporting Linux.

## Linux Gamers Can Be *Very* Different To Windows Gamers

Overshadowing those two points is the biggest reason why there is some truth to
the claim that you will receive a disproportionately higher numbers of reports
from Linux users: *Linux users are more likely to report issues with software
than non-Linux users*.

A computer user that would choose to install Linux is likely to be very
technically-proficient, even though using Linux does not itself require a high
level of technical proficiency. Because of that a higher proportion of Linux
gamers are likely to be technically-literate than Windows gamers.

The Linux platform itself has strong roots in the open-source movement, and
Linux users are frequent users of other open-source software. Supporters of
open-source software (and Linux users in general) typically are used to having a
closer relationship with developers compared to users of other software, often
spending time visiting the Githubs of software they use, leaving detailed bug
reports and attempting to reach out directly to a developer to offer helpful
information to improve the quality of software. When using software developed by
collaboration it might even be considered impolite to find a bug and not report
it so that it can be fixed. This different culture, which might be unexpected by
those used to only releasing closed software, can be an asset.

[Ana Betts, provided an amusing story of their experience in shipping software to Linux users:][2]

> As a contrasting anecdote, Slack shipped a Linux app based on Electron, it
accounted for a similar % of users as global OS share, and a similar amount of
tickets.
>
> In contrast though, the ticket *quality* of Linux users was through the roof -
they were super clear and actionable.
>
> ...
>
> Linux users were also *super* understanding that their weird config wasn't
supported, especially if you gave them Real Technical Info back in your support
response. They often would send us callstacks *and* a few times, even patch
Electron for us to fix it

When your software has bugs Linux users are more likely to report them, and
also more likely to offer higher quality bug reports, with more detailed
information.

With fully cross platform game development, this feature of the Linux market can
become extremely valuable, as these detailed bug reports can be useful for
identifying and fixing issues that affect all of your users across all
platforms.

[1]: https://www.forbes.com/sites/jasonevangelho/2019/08/07/porting-games-to-linux-is-a-waste-of-time-this-game-developer-says-youre-doing-it-wrong/#60cb0f0b2c16
[2]: https://mobile.twitter.com/anaisbetts/status/1082361023653326854
