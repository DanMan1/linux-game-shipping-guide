---
title: "Best Practices"
category: "General Advice"
order: 4
---

## Where To Store Game Data

The [XDG Base Directory Specification][2], in simple terms, describes where your
software should place user files on a Linux PC. Much like on Windows, game
configuration data and save game data should be stored in the appropriate
directories. Use `$XDG_DATA_HOME` for savegames and `$XDG_CONFIG_HOME` for
configuration files. Avoid creating files or folders directly in a user's `/home`
directory to prevent it from cluttering.

Storing all user data in a single folder, like [Unity][5] does by default
(`~/.config/unity3d/[Your Game]`), ought to be avoided, especially when combined
with cloud sync/storage. If users alternatingly play a game on very different 
hardware devices (handheld <> dektop PC), they'd have to constantly adjust their
graphics settings for each device after every sync.

> **Note:** Your game's code shouldn't assume that these variables are always set
and available. Always check if they are and fall back on the recommended
locations from the XDG spec, if they're not.

> **Note:** On Linux, the **~** character in a filepath refers to the user's 'home'
directory. For a user with the username `bob`, the filepath `~/file` would refer
to `/home/bob/file`. This is equilevant to `C:\Users\bob` on Windows.

## 64-bit Only

While there's no reason why you can't ship a 32-bit version of your game, it is
not necessary and is an additional burden you do not need to carry.

32-bit support is increasingly being phased out of the world of computing, and
this is true on Linux as well. [The number of 32-bit Linux installations][1] is
now so low that there is no benefit to choosing 32-bit over 64bit, or
supporting 32-bit alongside 64-bit.

## Dynamically Linked Libraries & Bundling Dependencies

Game developers on Windows will most likely be familiar with the system of DLLs,
aka dynamically linked libraries.

The equilivant system exists on Linux, and such libraries use the file extension
of ".so" and function in a similar manner.

It is common practice and recommended to bundle with your games on Linux your
library dependencies, the same as you would with Windows, and not depend on the
user's Linux distribution having either the libraries you need, or the same
version of those libraries you built, and tested your game against.

#### What to Bundle

As a general rule of thumb, you should include everything other than the
standard C/C++ runtime library (libc, libm, libdl, librt, libgcc_s, libstdc++,
etc), and any OpenGL/Vulkan libraries.

[More information on exactly what to bundle and not bundle][3] can be found in
this guide by an experienced expert on the subject.

#### Determining your Dependencies

To determine what dependencies your application has on Linux, you can use the
`ldd` command via the terminal on your executable to list it's dependencies.

[The `ldd` command explained with examples.][4]

#### LD_LIBRARY_PATH Environment Variable

LD_LIBRARY_PATH is a Linux environment variable which contains a list of
directories where the dynamic loader should look for .so files (libraries) when
executing an application, in priority order.

By manipulating the LD_LIBRARY_PATH environment variable, you can substitute
different .so files to be loaded by the executable. This is how you use the
libraries you have bundled with your application.

In games and applications, the most common method would be to use a wrapper
script for your game, set LD_LIBRARY_PATH with the current app's own lib
directory first, before the standard system locations like /usr/lib and /lib.

This lets the game or application bundle its own dependencies and use them in
preference to the same .so filename that may already exist on the Linux system.

```
#!/bin/sh
# Simple game executable wrapper script example.

# "." is the current working directory, so we put the bundled "./lib" on the
# library path before the previous contents of that environment variable.
export LD_LIBRARY_PATH=./lib:$LD_LIBRARY_PATH

# Run the game itself.
./bin/game
```


## Locale Handling

_TODO Need more information on this, including how to obtain locale
information for current language, keyboard input, timezone, etc, in native C++
applications, as well as in popular game engines. If you have knowledge on this
subject, please fork this project on Gitlab and send us a merge request with
some content on this subject to help get us started._

[1]: https://www.gamingonlinux.com/users/statistics
[2]: https://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html
[3]: https://gist.github.com/flibitijibibo/b67910842ab95bb3decdf89d1502de88
[4]: https://www.howtoforge.com/linux-ldd-command/
[5]: https://unity.com/
