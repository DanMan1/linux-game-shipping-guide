---
title: "Further Help"
category: "General Advice"
order: 9
---
If you have any further questions or concerns not addressed in this guide,
please reach out to the community. The following places are recommended for
help:

* Reddit: [r/linux_gaming](https://www.reddit.com/r/linux_gaming/), [r/gamedev](https://www.reddit.com/r/gamedev/)
* [GamingOnLinux](https://gamingonlinux.com)
