---
title: "Cross Platform Development"
category: "General Advice"
order: 2
---
## It is easier to remain cross platform than to become cross platform.

A game development project involves a lot of decision making, decisions such as
which graphics API to use, what library to use for controller input, and so on.

Decisions such as these can ultimately lock a project to a single platform if
the decision is made to use platform dependent technologies, such as DirectX.

Porting a Windows only game to another platform (Linux or otherwise) later on
becomes much harder as a result, as low level technologies in your game's code
then need to be replaced with new cross platform technologies. Depending on how
ingrained those technologies are in your game's code base, this could involve a
lot of rewriting of code, and also introduce bugs that weren't present before.

If the decision is made early on, to use a cross platform technology instead,
then porting to other platforms becomes a simple matter of compiling to another
target, reducing the cost and maximising the return and benefit of supporting
multiple platforms.

Making the right decisions early on in your development process to use
technologies which are platform independent, such as SDL or Vulkan, will also
allow you to bring your game more quickly and easily to other platforms, either
existing or new platforms that may emerge in the future of the always evolving
landscape of gaming.

## Compile for Linux as Soon As Possible

Test early, test often.

Developing a game for multiple platforms at the same time is an effective way
to smoke out bugs from your code. It's best to compile for all platforms as
early as possible, then continue to do so regularly, to reveal bugs as soon as
they are introduced. This is a net benefit for all platforms, as well-optimised
and bug-free code will be more reliable and perform better in the future should
that code need to be ported to even more platforms.

## Porting Existing Unreal/Unity/Godot Projects

The good news is that, if you need to port an existing Unreal/Unity/Godot game
engine based project, you're already 90% of the way there before you even start.

Engines like Unreal, Unity, and Godot, all support exporting to Linux out of the
box. The only 'gotcha' to keep an eye out for is any middleware or plugins you
may be using that might be incompatible with Linux, which will need to be
replaced with Linux compatible middleware that serves the same function.

## Porting an Existing C++ Project

If you're porting a project that was written in C++ and directly uses Windows
specific APIs or libraries, such as Direct3D, DirectInput, etc, the process of
porting your project will be a lot more involved, but still very worth persuing
for the benefits. Not just the benefit of having a Linux compatible build, but
also making it easier to port your game to other platforms in the future, such
as MacOS or game consoles or cloud gaming services.

Here's advice from two experienced Linux game porters with many years of
experience to aid you in getting started with this task.

#### Advice from Ryan Gordon

Quoting [this excellent presentation](https://www.youtube.com/watch?v=Sd8ie5R4CVE)
from 2014 by Ryan Gordon that you should definitely watch:

> The zen of this is: porting a game is just overcoming obstacles until you
run out of obstacles to overcome.

Ryan describes his process as:

 1. Start on Windows. Replace Windows specific code with SDL. Add/Replace your
 existing Direct3D code with a cross platform graphics API, such as Vulkan.
 This will cover 90% of the work of making your project crossplatform.
 2. Use G++ or Clang. Switch to a cross platform build system
 (CMake, Scons, etc).
 3. Switch to Linux. Start trying to compile your project. It will fail of
 course the first time.
 4. Start going through the project at the places where the code is crashing or
 having problems, anything that prevents the code from compiling: **Stub it out.**
 Continue doing so until your project can compile.
 5. Once your code is compiling and running without crashes, start fixing the
 simple problems. Eg: Replace a single line call to the Windows message textbox
 with a similar function from SDL.
 6. Get it linking, get your middleware sorted out, get the Linux versions for
 anything you need, replace any dependencies that are Windows only with cross
 platform dependencies.
 7. Get it running! Fix any outstanding issues.
 8. Debug it, optimise it.
 9. Merge your new cross platform code base back into your original code base.
 Now your game can be compiled to either Windows or Linux, and you do not have
 two codebases to maintain.

For your Windows specific code, Ryan advises using SDL as it takes care of 90%
of everything you are using Windows specific code for already. Creating windows,
simple message boxes, drawing surfaces, controller input, etc.

Be sure to watch the full length video for more wonderful insights from Ryan.

#### Advice from Valiant Cheese

To quote the brilliant and knowledgeable Linux porter 'Valiant Cheese', from
their wonderful [presentation](https://www.youtube.com/watch?v=d8kfva6G0c4),
the process of porting an existing game can be summed up as:

 1. Get it compiling
    * Set up a build environment
    * Replace problematic dependencies
    * Stub out problematic code.
 2. Fix it up
    * Write/rewrite platform specific implementation for stubbed code
    * Address platform specific bugs
    * Implement platform specific best practices
 3. Package it
    * Bundle dependencies
    * Build package manager dependencies (optional)
    * Make setup application (optional)
    * Write launch script (optional)
    * Tar it up (optional)

Cheese goes into depth on each of these points in his presentation and for more
detail it's advised to watch his wonderful presentation in full for his
fantastic insights.
